<?php

class User
{
    private $username;
    private $email;

    public function __construct($username, $email)
    {
        $this->username=$username;
        $this->email=$email;
    }

    public function addFriend()
    {
        return "$this->username  added a friend";
    }


    // getter
    public function getEmail()
    {
        return $this->email;
    }

    // setter
    public function setEmail($email)
    {
        if (strpos($email, '@')>-1) {
            $this->email=$email;
        }
    }
}

class AdminUser extends User
{
    public $level;
    public function __construct($username, $email, $level)
    {
        $this->level=$level;
    }
}

$user_1 = new User('Gunther', 'gunther@email.de');
$user_2 = new AdminUser('Clara', 'clara@email.de', 5);
$user_1->setEmail('test@email.de');

echo $user_1->getEmail() ."<br>";
echo $user_1->addFriend() ."<br>";
echo "--------------<br>";

$user_2->setEmail('clara@emailaccount.de');
echo $user_2->getEmail() . "<br>";
echo $user_2->level;
