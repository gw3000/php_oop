<?php
require_once 'inputs.php';

// could be result of a sql query
$text1 = 'erster Textinhalt';
$text2 = 'zweiter Textinhalt';
$text3 = 'dritter Textinhalt';

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>OOP</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>

<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="container">
    <div class="row">
      <div class="one-half column" style="margin-top: 5%">
        <h4>Skeleton and PHP OOP</h4>
        <form>
          <div class="row">
            <?php
                $email_1 = new Email('Email-Adresse', 'email1');
                echo $email_1->email();

                $textarea_1 = new Textarea('erster Text', 't1', $text1);
                echo $textarea_1->textarea();

                $textarea_2 = new Textarea('zweiter Text', 't2', $text2);
                echo $textarea_2->textarea();

                $textarea_3 = new Textarea('dritter Text', 't3', $text3);
                echo $textarea_3->textarea();

                //print_r(Weather::$tempConditions);

                $gradCelsius = 10;
                echo $gradCelsius . "°C entspricht "
                    . Weather::celsiusToFahrenheit($gradCelsius). "°F<br>";

                $gradFahrenheit = 70;
                echo $gradFahrenheit . "°F fühlt sich "
                    . Weather::determineTempConditions($gradFahrenheit)
                    . " an. <br>";
            ?>
          </div>
        </form>
      </div>
    </ div>

    <!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>

 </html>
