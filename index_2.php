<?php

// public
// private
// protected

class Employee extends Person
{
    private $jobTitle;
    public function __construct($jobTitle, $firstName, $lastName, $gender = 'f')
    {
        // $this->jobTitle = $jobTitle;
        $this->setJobTitle($jobTitle);

        parent::__construct($firstName, $lastName, $gender);
    }

    public function setJobTitle($jobTitle = null)
    {
        $this->jobTitle = ucfirst($jobTitle);
    }

    public function getJobTitle()
    {
        return $this->jobTitle;
    }
}

class Person
{
    public $firstName;
    public $lastName;
    public $gender;

    public function __construct($firstName, $lastName, $gender = 'f')
    {
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->gender = $gender;
    }

    public function sayHello()
    {
        return "Hello my name is " . $this->firstName . " " . $this->lastName;
    }


    public function getGender()
    {
        if ($this->gender == 'f') {
            $g = 'female';
        } elseif ($this->gender == 'm') {
            $g = 'male';
        }
        return $g;
    }
}


$gw = new Person('Gunther', 'Weissenbaeck', 'm');
echo $gw->firstName . " " . $gw->lastName;
echo "\n";
echo $gw->sayHello();

// $ms = new Employee('frontend Developer', 'Marry', 'Stewart');
// echo "\n";
// echo $ms->firstName . " " . $ms->lastName;
// echo "\n";
// echo $ms->getGender();
// echo "\n";
// echo $ms->sayHello();
// echo "\n";
// echo $ms->getJobTitle() . "\n";
// $ms->setJobTitle('backend Developer');
// echo $ms->getJobTitle();

// https://www.youtube.com/watch?v=fiMo0zNdrt4
