<?php


class Element
{
    protected $title;
    protected $id;
    public function __construct($title, $id)
    {
        $this->title = $title;
        $this->id = $id;
    }
}

class Textarea extends Element
{
    private $text;

    public function __construct($title, $id, $text)
    {
        $this->text = $text;
        parent::__construct($title, $id);
    }

    public function textarea()
    {
        $textarea = "<label for='$this->id'>$this->title</label>
            <textarea class='u-full-width' id='$this->id'>$this->text</textarea>";
        return $textarea;
    }
}


class Email extends Element
{
    public function __construct($title, $id)
    {
        parent::__construct($title, $id);
    }

    public function email()
    {
        $email = "<label for='$this->id'>$this->title</label>
            <input class='u-full-width' type='email' placeholder='email@email.de' id='$this->id'>";
        return $email;
    }
}

// static properties and methods
class Weather
{
    public static $tempConditions = ['cold', 'mild', 'warm'];

    public static function celsiusToFahrenheit($c)
    {
        return $c *9 /5 +32;
    }

    public static function determineTempConditions($f)
    {
        if ($f < 40) {
            return self::$tempConditions[0];
        } elseif ($f < 70) {
            return self::$tempConditions[1];
        } else {
            return self::$tempConditions[2];
        }
    }
}
