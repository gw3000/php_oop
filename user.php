<?php
require_once 'user_validation.php';

if (isset($_POST['submit'])) {
    $validation = new UserValidation($_POST);
    $errors = $validation->validateForm();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>User Validation</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/styles.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>

<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="container">
    <div class="row">
      <div class="one-half column" style="margin-top: 5%">
        <h4>Neuen Benutzer anlegen</h4>
          <div class="row">
          <form  action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
            <label for="username">Benutzername:</label>
            <input type="text" name="username" value= "<?php echo htmlspecialchars($_POST['username'])?>">
            <div class="error">
            <?php echo $errors['username'];?>
            </div>

            <label for="email">Email-Adresse:</label>
            <input type="text" name="email" value = "<?php echo htmlspecialchars($_POST['email'])?>">
            <div class="error">
            <?php echo $errors['email'];?>
            </div>

            <input type="submit" value="submit" name="submit">
        </form>
          </div>
      </div>
    </ div>

    <!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>

 </html>
