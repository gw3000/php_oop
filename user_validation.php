<?php


// create a user validator class to handle validation
// constructior which takes in POST data from form
// check required  "fields to check" are present in the data
// create methods to validate individual fields
// - a method to validate a username
// - a method to validate ann email
// return an error array one all checkes are done
class UserValidation
{
    private $data;
    private $errors = [];
    private static $fields = ['username', 'email'];

    public function __construct($post_data)
    {
        $this->data=$post_data;
    }

    public function validateForm()
    {
        foreach (self::$fields as $field) {
            if (!array_key_exists($field, $this->data)) {
                trigger_error("$field is not present in data");
                return;
            }
        }
        $this->validateUsername();
        $this->validateEmail();
        return $this->errors;
    }

    private function validateUsername()
    {
        $val = trim($this->data['username']);
        if (empty($val)) {
            $this->addError('username', 'Benutzername darf nicht leer sein!');
        } else {
            if (!preg_match('/[a-zA-Z0-9]{6,12}$/', $val)) {
                $this->addError('username', 'muss 6-12 alphanumerische Zeichen enthalten!');
            }
        }
    }

    private function validateEmail()
    {
        $val = trim($this->data['email']);

        if (empty($val)) {
            $this->addError('email', 'Email-Adresse darf nicht leer sein!');
        } else {
            if (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                $this->addError('email', 'muss eine gültige Email-Addresse sein');
            }
        }
    }

    private function addError($key, $val)
    {
        $this->errors[$key]=$val;
    }
}
